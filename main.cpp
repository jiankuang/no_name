#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <map>
#include <vector>
#include <algorithm>
using namespace std;

extern "C" {
    #include "occ/graph.h"
    #include "occ/callOCC.h"
}

typedef struct Node{
    int index, w = 1;
    string name;
    vector<Node *> constraintP;  
} Node;

bool compNode(Node *a, Node *b)
{
    return (a->constraintP.size() < b->constraintP.size());
}


#define BUFFERSIZE 256

#define _getOneLine \
    while(true)     \
    {if(fgets(line, BUFFERSIZE, fp) && strlen(line)>0 && line[0]!='#') break;}

#define _getOneLineOfNum \
    while(true)          \
    {if(fgets(line, BUFFERSIZE, fp) && strlen(line)>0 && line[0]<='9' && line[0]>='0') break;}

extern int callWOCC(struct ograph *g, int *weight, size_t *result);

int main(int argc, char **argv)
{
	if(argc<3 || !(atoi(argv[2]) ==1 || atoi(argv[2]) ==2))
    {
		cout<<"usage: "<<argv[0]<<" <benckmark_file>  <TYPE(1=weight/2=unweighted)>   "<<endl;
		exit(-1);
	}

    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("cannot open file %s\n", argv[1]);
        exit(-1);
    }    
    int type = atoi(argv[2]);

    int nNode, nEdge;
    char line[BUFFERSIZE];
    char name[BUFFERSIZE];
    char n1[BUFFERSIZE];
    char n2[BUFFERSIZE];
    map<string,int> nodeID;

	_getOneLineOfNum;    
    sscanf(line, "%d\n", &nNode);
    _getOneLine;    
    sscanf(line, "%d\n", &nEdge);

    int weight[nNode];
    size_t result[nNode];
    Node nodeArr[nNode];
    Node* nodes[nNode];
    for(int i=0; i<nNode; i++)
        nodes[i] = &nodeArr[i];

    for(int k = 0; k<nNode; k++)
    {
		_getOneLine;
		if(type==1)
		{
			int w = 1;
    		sscanf(line, "%s%d\n", name, &w);
            nodes[k]->w = w;
		}
		else if(type==2)
		{
    		sscanf(line, "%s\n", name);
		}
		nodeID[string(name)] = k;
        nodes[k]->name = string(name);
    }
    printf("nNode %d\n", nNode);
    printf("nEdge %d\n", nEdge);


    for(int i=0; i<nEdge; i++)
    {
        _getOneLine;
        sscanf(line, "%s%s\n", n1, n2);
        int id1 = nodeID[string(n1)];
        int id2 = nodeID[string(n2)];
        nodes[id1]->constraintP.push_back(nodes[id2]);
        nodes[id2]->constraintP.push_back(nodes[id1]);
    }
    //we want to handle low-degree node first
    sort(nodes, nodes+nNode, compNode);

    if(nodes[0]->constraintP.size()==0)
    {
        printf("Vertex (%s) is isolated. Exit\n", nodes[0]->name.c_str());
        return -1;
    }

    for(int i=0; i<nNode; i++)
    {
        nodes[i]->index = i;
        weight[i] = nodes[i]->w;
    }

    //this is the data structure supported in callWOCC
    struct ograph *g = graph_make(nNode);
    for(int i=0; i<nNode; i++)
    {
        for(auto n : nodes[i]->constraintP)
        {
            int j = n->index;
            if(j > i)
            {
                graph_connect(g, i, j);
            }
        }
    }

    fclose(fp);
    // graph_output(g, stdout, NULL);

    int n = 0;
    if(type==1)
    {
    	n = callWOCC(g, weight, result);
    	cout<<"wocc: "<<n<<" vertices\n";
    }
    else if(type==2)
    {
        //this will call the occ algorithm described in 
        //"Algorithm engineering for optimal graph bipartization" (the fastest version, unweighted)
    	n = callOCC(g, result, true, true);
    	cout<<"occ: "<<n<<" vertices\n";
    }
    for(int i=0; i<n; i++)
    {
    	cout<<nodes[result[i]]->name<<' ';
    }
    cout<<endl;

    if(type==1)
    {
        int totWeight = 0;
        for(int i=0; i<n; i++)
        {
            totWeight += nodes[result[i]]->w;
        }
        printf("total weight = %d\n", totWeight);
    }

	return 0;
}
