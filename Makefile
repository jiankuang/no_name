ifeq ($(mode),debug)
    OPT= -O0 -ggdb
    LIBS = -pthread -ldl
else
ifeq ($(mode),profile)
    OPT= -O0 -pg
    LIBS = -pthread -ldl
else
ifeq ($(mode),release)
    OPT= -O4 -DNDEBUG -static
    LIBS = -pthread -ldl -std=c++11
else
    OPT= -O4 -DNDEBUG -Wno-write-strings
    LIBS = -pthread -ldl
endif
endif
endif

#WFLAG= -Wall -Werror

CC= g++-4.8 $(OPT) $(TYPE) $(PROFILE)
CC11= g++-4.8 -std=c++11 $(OPT) $(TYPE) $(PROFILE)
CC99 = gcc-4.8 -std=c99 $(OPT) $(TYPE) $(PROFILE)
AR= ar rcs
TAG= ctags

OBJS = main.o 	flow/graph.o    flow/maxflow.o    callWOCC.o		

SOURCES	= \
	occ/bitvec.c	\
	occ/flow.c		\
	occ/graph.c		\
	occ/callOCC.c		\
	occ/occ-enum2col.c	\
	occ/occ-gray.c	\
	occ/occ.c		\
	occ/util.c

OCC_OBJS	= $(SOURCES:.c=.o)

SRCS = ${OBJS:%.o=%.cpp}
BFILE = optWOCC

.PHONY: all
all: $(BFILE)

$(BFILE): $(OBJS) $(OCC_OBJS)
	$(CC) -o $(BFILE) $(OBJS) $(OCC_OBJS) -lm -ldl $(LIBS)  

%.o: %.c
	$(CC99) -o $*.o -c $<

%.o : %.cpp %.h
	$(CC11) -o $*.o -c $*.cpp

%.o : %.cpp
	$(CC11) -o $*.o -c $*.cpp

.PHONY: clean
clean:
	rm -f $(OBJS) 

.PHONY: tags
tags:
	cscope -Rbq
	ctags -R *.cpp *.h

