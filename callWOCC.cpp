#include <stdio.h>
#include <iostream>
#include <cmath>
#include <ctime>
#include <vector>
#include "./flow/graph.h"

extern "C" {
    #include "./occ/graph.h"
    #include "./occ/callOCC.h"
    #include "./occ/occ.h"
    #include "./occ/bitvec.h"
    #include "./occ/util.h"
    #include "./occ/flow.h"
}
using namespace std;

#define largew 10000000

bool verb = false;

int CAPS[];
int CAP_CNT;

static bool graph_two_coloring(const struct ograph *g, char *colors) {
    size_t size = graph_size(g);
    ALLOCA_BITVEC(seen, size);
    vertex queue[size];
    vertex *qhead = queue, *qtail = queue;

    for (size_t v0 = 0; v0 < size; v0++) {
	if (!graph_vertex_exists(g, v0) || bitvec_get(seen, v0))
	    continue;
	assert(qtail <= queue + size);
	*qtail++ = v0;
	bitvec_set(seen, v0);
	do {
	    vertex v = *qhead++, w;
	    char c = colors[v];
	    GRAPH_NEIGHBORS_ITER(g, v, w) {
			if (!bitvec_get(seen, w)) {
			    colors[w] = !c;
			    assert(qtail < queue + size);
			    *qtail++ = w;
			    bitvec_set(seen, w);
			} else {
			    if (colors[w] == c)
				return false;
			}
	    }
	    
	} while (qhead != qtail);
    }
    assert (bitvec_count(seen) == graph_num_vertices(g));
    return true;
}

bool occ_is_occ(const struct ograph *g, const struct bitvec *occ, char *colors) {
	memset(colors, 0, g->size);
    assert(g->size == occ->num_bits);
    ALLOCA_U_BITVEC(not_occ, g->size);
    bitvec_copy(not_occ, occ);
    bitvec_invert(not_occ);    
    struct ograph *g2 = graph_subgraph(g, not_occ);
    bool occ_is_occ = graph_two_coloring(g2, colors);
    graph_free(g2);
    return occ_is_occ;
}

#define addEdge(v, w) \
		g->add_edge(v+size2, w, largew, 0); \
		g->add_edge(w+size2, v, largew, 0); 


/* This function implements W_Compress algorithm
 * gin: input graph G
 * occ: current occ X
 * weight: weight of nodes
 * char *color: color of the nodes
 * G-X is 2-colorable, and char *color already contains the colors */

static struct bitvec* W_Compress(struct ograph *gin, struct bitvec *occ, int *weight, char *colors)
{
	CAP_CNT = 0;
	int size = graph_num_vertices(gin);

    int x[bitvec_count(occ)]; //x contains the nodes in current occ
    int xn = 0;
    int totOldW = 0;

    for (size_t i = bitvec_find(occ, 0); i != BITVEC_NOT_FOUND; i = bitvec_find(occ, i + 1))
    {
    	colors[i] = 2;
    	x[xn++] = i;
    	totOldW += weight[i];
    }
    int targetW = totOldW - weight[x[xn-1]];
    //a smaller occ's weight is at least targetW, 
    //the searching can stop once such weight is obtained




    /* SECTION START: construct G' from G */

    typedef Graph<int,int,int> GraphType;
    int size2 = size * 2;
	int n = size2 * 2;
	//1. given a node v, we have a dummy edge v->v+size2, where the edge cost = v's cost
	//v is the input node, v+size2 is the output node
	//this is to make the graph suitable for flow solver
	//2. given a node v in X, we have v and v+size, i.e., each node in X has two copies

	int nArc = (size + xn) + (xn * 4) + (graph_num_edges(gin) * 2);
	//we have at most so many edges
	GraphType *g = new GraphType(n, nArc);
	g -> add_node (n);
	int map_v_to_arc[size];

	for (unsigned v = 0; v < size; v++) 
    {
		if(colors[v]==2)
		{
			g->add_edge(v, v+size2, 0, 0);//add 2 arcs
			map_v_to_arc[v] = g->get_arc_num()-2;
			g->add_edge(v+size, v+size+size2, 0, 0);//add 2 arcs
		}
		else
		{
			g->add_edge(v, v+size2, weight[v], 0);
		}
	}

    for (unsigned v = 0; v < size; v++) 
    {
    	for (unsigned n = 0; n < gin->vertices[v]->deg; n++) 
    	{
			unsigned w = gin->vertices[v]->neighbors[n];
			if (v < w) 
			{
				// printf("v %d w %d\n", v, w);
				if(colors[v]==0)
				{
					if(colors[w]==0)
					{
						printf("ERROR: color error\n");
						getchar();
					}
					else if(colors[w]==1)
					{
						addEdge( v, w );
					}
					else if(colors[w]==2)
					{
						addEdge( v, w + size );
					}
				}
				else if(colors[v]==1)
				{
					if(colors[w]==1)
					{
						printf("ERROR: color error\n");
						getchar();
					}
					else if(colors[w]==0)
					{
						addEdge( v, w );
					}
					else if(colors[w]==2)
					{
						addEdge( v, w );
					}
				}
				else if(colors[v]==2)
				{
					if(colors[w]==0)
					{
						addEdge( v+size, w );
					}
					else if(colors[w]==1)
					{
						addEdge( v, w );
					}
					else if(colors[w]==2)
					{
						addEdge( v, w+size );
					}
				}
			}
    	}
    }
    
    /* SECTION END: construct G' from G */





	int minCost = totOldW;
	struct bitvec * minOCC = NULL;

    int iter1 = pow(2, xn);
    int start1 = iter1/2;
	int code1[xn];
	int j;

	//this iteration iterates each Y \subset of X.
	//code1[j]=1 ==> j is in Y,   code1[j]=0 ==> j is not in Y.
	//we only consider half of the iterations,
	//because the last node added to X must appear in Y,
	//i.e., the last bit of code1[] must be 1.
	for(int ii=start1; ii<iter1; ii++)
    {
    	if(ii==start1)
    	{
    		//the first code1[] we need to consider
    		code1[xn-1] = 1;
    		for(j=0; j<xn-1; j++)
    			code1[j] = 0;
    	}
    	else
    	{
    		//increase code1[] by 1
    		code1[0]++;
    		j=0;
    		while(code1[j]>1)
    		{
    			code1[j]=0;
    			code1[++j]++;
    		}
    	}

    	int Y[xn], posInY[size]; //position in Y
    	fill(posInY, posInY+size, -1);
    	int yn = 0;

    	//now according to code1, we get the set Y
    	for(j=0; j<xn; j++)
    	{
    		if(code1[j]==1)
    		{
    			Y[yn] = x[j];
    			posInY[x[j]] = yn;
    			yn++;
    		}
    	}


    	#if SPEEDUP_BY_COLOR_Y
    	//this section tests if G[y] is not 2-colorable, and continue if so
    	//it takes extra time to test G[y]
    	//so this will be beneficial only when the graph is very dense
    	ALLOCA_BITVEC(sub, size);
    	for(j=0; j<xn; j++)
    	{
    		if(code1[j]==1)
    		{
    			bitvec_set(sub, x[j]);
    		}
    	}
    	struct ograph *g2 = graph_subgraph(gin, sub);
	    if(graph_is_bipartite(g2)==false) {
	    	graph_free(g2);
	    	continue;
	    }
	    graph_free(g2);
	    #endif


    	typedef pair<int,int> pint;
    	vector<pint> neibs; 
    	for (size_t v = 0; v < size; v++)
    	{
    		if(posInY[v]!=-1)
    		{
	        	for (size_t n = 0; n < gin->vertices[v]->deg; n++)
	        	{
					int w = gin->vertices[v]->neighbors[n];
					if(w>v && posInY[w]!=-1)
					{
						neibs.push_back(pint(posInY[v], posInY[w]));
					}
				}
			}
		}

    	int iter2 = pow(2, yn);
		int code[yn];
		fill(code, code+yn, 0);

		//this iteration iterates each valid division of Y.
		//code[i]=1 ==> i is in YA,   code[i]=0 ==> i is in YB.
		//we only consider half of the divisions,
		//because the other half is symetric
		for(int i=0; i<iter2/2; i++)
		{
			if(i!=0)
			{
				//increse code by 1
    			code[0]++;
	    		j=0;
	    		while(code[j]>1)
	    		{
	    			code[j]=0;
	    			code[++j]++;
	    		}
    		}
	       
			//if neiboring nodes are in the same partition of Y_A and Y_B, skip this division	       
        	for(j=0; j<neibs.size(); j++)
        	{
        		if(code[neibs[j].first]==code[neibs[j].second])
        		{
        			goto SKIP;
        		}
        	}

			if(false) //has effect only when it comes from "goto SKIP"
			{
			SKIP:
				continue;
			}

    		g->reuse_cap();
    		g->reset_flow();

	    	int oldW = 0;
	    	
	    	for(j=0; j<yn; j++)
	    	{
	    		int v = Y[j];
	    		if(code[j]==0)
	    		{
	    			g -> add_tweights( v, largew, 0 );
	    			g -> add_tweights( v+size+size2, 0, largew );
	    		}
	    		else if(code[j]==1)
	    		{
	    			g -> add_tweights( v+size2, 0, largew );
	    			g -> add_tweights( v+size, largew, 0 );
	    		}

    			g->set_cap(map_v_to_arc[v], weight[v]);
    			g->set_cap(map_v_to_arc[v]+2, weight[v]);

    			oldW += weight[v];
	    	}

	    	int flow = g -> maxflow();
	    	// printf("flow %d\n", flow);
	    	if(flow < oldW)
	    	{
	    		//weight(x) - weight(y) + weight(cutSet)
	    		int cost = totOldW - oldW + flow;
	    		//found a smaller occ
	    		if(cost < minCost)
	    		{
	    			minCost = cost;
	    			if(minOCC) bitvec_free(minOCC);
	    			minOCC = bitvec_clone(occ);
	    			for(j=0; j<yn; j++)
			    	{
			    		bitvec_unset(minOCC, Y[j]);
			    	}
	    			for (unsigned v = 0; v < size; v++) 
				    {
				    	//if v is in the cut set
				    	//i.e., v->v+size2 is cut by max-flow min-cut
				    	//(if v is in X, then we consider v+size->v+size+size2)
						if (g->what_segment(v) != g->what_segment(v+size2)
							|| (colors[v]==2 && g->what_segment(v+size) != g->what_segment(v+size+size2)))
						{
							bitvec_set(minOCC, v);
						}
					}
					if(cost <= targetW)
					{
						goto FINISH;
					}
	    		}
	    	}

	    	for(j=0; j<yn; j++)
	    	{
	    		int v = Y[j];
	    		if(code[j]==0)
	    		{
	    			g->set_trcap(v, 0);
	    			g->set_trcap(v+size+size2, 0);
	    		}
	    		else if(code[j]==1)
	    		{
	    			g->set_trcap(v+size2, 0);
	    			g->set_trcap(v+size, 0);
	    		}
	    	}
    	}
    }

FINISH:

	delete g;
	return minOCC;
}

static struct bitvec *find_occ(const struct ograph *g, int *weight) 
{
    struct bitvec *occ = NULL;
    int cnt = 0;

    occ = bitvec_make(g->size);
    ALLOCA_BITVEC(sub, g->size);
	char colors[g->size], colors2[g->size];
	memset(colors, 0, g->size);

    for (size_t i = 0; i < g->size; i++) 
    {
		bitvec_set(sub, i);

		bool used0 = false, used1 = false;
		for (size_t n = 0; n < g->vertices[i]->deg; n++) 
		{
			int j = g->vertices[i]->neighbors[n];
			if (j < i)
			{
				if(colors[j]==0) used0 = true;
				if(colors[j]==1) used1 = true;
				if(used1 && used0) break;
		    }
		}
		if(used0==false)
		{
			colors[i] = 0;
			continue;
		}
		else if(used1==false)
		{
			colors[i] = 1;
			continue;
		}

		struct ograph *g2 = graph_subgraph(g, sub);
		//if current occ is already a valid occ for g2, skip W-Compress
		if (occ_is_occ(g2, occ, colors2)) 
		{
		    graph_free(g2);
		    for(int ii=0; ii<=i; ii++)
		    	colors[ii] = colors2[ii];
		    continue;
		}

		   
		bitvec_set(occ, i);
		struct bitvec *occ_new =  W_Compress(g2, occ, weight, colors);

		if (occ_new) 
		{
			// Found a smaller OCC
		    free(occ);
		    occ = occ_new;
		    if (!occ_is_occ(g2, occ, colors)) 
		    {
				fprintf(stderr, "Internal error!\n");
				abort();
		    }
		}
		graph_free(g2);
    }
    return occ;
}

int callWOCC(struct ograph *g, int *weight, size_t *result) 
{
    size_t occ_size;

    struct bitvec *occ = find_occ(g, weight);

    occ_size = bitvec_count(occ);

    int i = 0;
	BITVEC_ITER(occ, v)
	{
		result[i++] = v;
	}

    return occ_size;
}

