#include <stdio.h>
#include <stdlib.h>

#if defined (__SVR4) && defined (__sun)
int getopt(int argc, const char *argv[], const char *optstring);
#else
# include <getopt.h>
#endif
#include <unistd.h>
#include <sys/times.h>
#include "bitvec.h"
#include "graph.h"
#include "occ.h"

bool enum2col   = false;
bool use_gray   = false;
bool verbose    = false;
bool stats_only = false;
unsigned long long augmentations = 0;

struct bitvec *find_occ(const struct ograph *g) {
	struct bitvec *occ = NULL;
	
	occ = bitvec_make(g->size);
	ALLOCA_BITVEC(sub, g->size);

	for (size_t i = 0; i < g->size; i++) {
		bitvec_set(sub, i);
	// bitvec_dump(sub); printf("\n");
	// bitvec_dump(occ); printf("\n");
		struct ograph *g2 = graph_subgraph(g, sub);
		if (occ_is_occ(g2, occ)) {
			graph_free(g2);
			continue;
		}
		bitvec_set(occ, i);
		if (verbose) {
			fprintf(stderr, "size = %3lu ", (unsigned long) graph_num_vertices(g2));
			fprintf(stderr, "occ = ");
			bitvec_dump(occ);
			putc('\n', stderr);
		}
		struct bitvec *occ_new = occ_shrink(g2, occ, enum2col,
			use_gray, true);
		if (occ_new) {
			free(occ);
			occ = occ_new;
			if (!occ_is_occ(g2, occ)) {
				fprintf(stderr, "Internal error!\n");
				abort();
			}
		}
		graph_free(g2);
	}
	return occ;
}

int callOCC(struct ograph *g, size_t *result, bool _enum2col, bool _use_gray) {
	size_t occ_size;
	enum2col = _enum2col;
	use_gray = _use_gray;

	struct bitvec *occ = find_occ(g);

	occ_size = bitvec_count(occ);
	int i = 0;

	BITVEC_ITER(occ, v)
	result[i++] = v;
	    // puts(vertices[v]);
	
	// printf("%5lu %6lu %5lu %10.2f %16llu\n",
	//        (unsigned long) g->size, (unsigned long) graph_num_edges(g),
	//        (unsigned long) occ_size, user_time(), augmentations);

	return occ_size;
}
